package pe.usil.android.recolectorapp.Helper;

/**
 * Created by Piero on 07/11/2017.
 */

public class Mensaje {
    private String mensaje;
    private String urlFoto;
    private String nombre;
    private String fotoPerfil;
    private String tipoMensaje;
    private boolean isResponse;

    public Mensaje() {
    }

    public Mensaje(String mensaje, String urlFoto, String nombre, String fotoPerfil, String tipoMensaje, boolean isResponse) {
        this.mensaje = mensaje;
        this.urlFoto = urlFoto;
        this.nombre = nombre;
        this.fotoPerfil = fotoPerfil;
        this.tipoMensaje = tipoMensaje;
        this.isResponse = isResponse;
    }

    public Mensaje(String mensaje, String nombre, String fotoPerfil, String tipoMensaje, boolean isResponse) {
        this.mensaje = mensaje;
        this.nombre = nombre;
        this.fotoPerfil = fotoPerfil;
        this.tipoMensaje = tipoMensaje;
        this.isResponse = isResponse;
    }

    public String getMensaje() {
        return mensaje;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }

    public String getUrlFoto() {
        return urlFoto;
    }

    public void setUrlFoto(String urlFoto) {
        this.urlFoto = urlFoto;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getFotoPerfil() {
        return fotoPerfil;
    }

    public void setFotoPerfil(String fotoPerfil) {
        this.fotoPerfil = fotoPerfil;
    }

    public String getTipoMensaje() {
        return tipoMensaje;
    }

    public void setTipoMensaje(String tipoMensaje) {
        this.tipoMensaje = tipoMensaje;
    }

    public boolean getIsResponse() {
        return isResponse;
    }

    public void setIsResponse(boolean isResponse) {
        this.isResponse = isResponse;
    }
}
