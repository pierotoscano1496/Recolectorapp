package pe.usil.android.recolectorapp.Helper;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bumptech.glide.Glide;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import pe.usil.android.recolectorapp.HolderMensaje;
import pe.usil.android.recolectorapp.R;

/**
 * Created by Piero on 07/11/2017.
 */

public class AdapterMensajes extends RecyclerView.Adapter<HolderMensaje> {
    private List<MensajeRecibir> listMensaje = new ArrayList<MensajeRecibir>();
    private Context context;

    public AdapterMensajes(Context context) {
        this.context = context;
    }

    public void addMensaje(MensajeRecibir mssgRecep) {
        listMensaje.add(mssgRecep);
        notifyItemInserted(listMensaje.size());
    }

    @Override
    public HolderMensaje onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.card_view_mensaje, parent, false);
        return new HolderMensaje(view);
    }

    @Override
    public void onBindViewHolder(HolderMensaje holder, int position) {
        if (listMensaje.get(position).getIsResponse()) {
            holder.getCvContenedor().setCardBackgroundColor(Color.parseColor("#E1FFD6"));
        }
        holder.getTxtNombreUsuarioMensaje().setText(listMensaje.get(position).getNombre());
        holder.getTxtMensajeMensaje().setText(listMensaje.get(position).getMensaje());
        if (listMensaje.get(position).getTipoMensaje().equals("2")) {
            holder.getImgMensajeFoto().setVisibility(View.VISIBLE);
            holder.getTxtMensajeMensaje().setVisibility(View.VISIBLE);
            Glide.with(context).load(listMensaje.get(position).getUrlFoto()).into(holder.getImgMensajeFoto());
        } else if (listMensaje.get(position).getTipoMensaje().equals("1")) {
            holder.getImgMensajeFoto().setVisibility(View.GONE);
            holder.getTxtMensajeMensaje().setVisibility(View.VISIBLE);
        }
        if (listMensaje.get(position).getFotoPerfil().isEmpty()) {
            holder.getImgFotoPerfilMensaje().setImageResource(R.drawable.cust_serv);
        } else {
            Glide.with(context).load(listMensaje.get(position).getFotoPerfil()).into(holder.getImgFotoPerfilMensaje());
        }
        Long codigoHora = listMensaje.get(position).getHora();
        Date date = new Date(codigoHora);
        SimpleDateFormat dateFormat = new SimpleDateFormat("hh:mm:ss a");
        holder.getTxtHoraMensaje().setText(dateFormat.format(date));
    }

    @Override
    public int getItemCount() {
        return listMensaje.size();
    }
}
