package pe.usil.android.recolectorapp;

import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by Piero on 07/11/2017.
 */

public class HolderMensaje extends RecyclerView.ViewHolder {
    private CardView cvContenedor;
    private TextView txtNombreUsuarioMensaje;
    private TextView txtHoraMensaje;
    private TextView txtMensajeMensaje;
    private CircleImageView imgFotoPerfilMensaje;
    private ImageView imgMensajeFoto;

    public HolderMensaje(View itemView) {
        super(itemView);
        cvContenedor = (CardView) itemView.findViewById(R.id.cvContenedor);
        txtNombreUsuarioMensaje = (TextView) itemView.findViewById(R.id.txtNombreUsuarioMensaje);
        txtHoraMensaje = (TextView) itemView.findViewById(R.id.txtHoraMensaje);
        txtMensajeMensaje = (TextView) itemView.findViewById(R.id.txtMensajeMensaje);
        imgFotoPerfilMensaje = (CircleImageView) itemView.findViewById(R.id.imgFotoPerfilMensaje);
        imgMensajeFoto = (ImageView) itemView.findViewById(R.id.imgMensajeFoto);
    }

    public CardView getCvContenedor() {
        return cvContenedor;
    }

    public void setCvContenedor(CardView cvContenedor) {
        this.cvContenedor = cvContenedor;
    }

    public TextView getTxtNombreUsuarioMensaje() {
        return txtNombreUsuarioMensaje;
    }

    public void setTxtNombreUsuarioMensaje(TextView txtNombreUsuarioMensaje) {
        this.txtNombreUsuarioMensaje = txtNombreUsuarioMensaje;
    }

    public TextView getTxtHoraMensaje() {
        return txtHoraMensaje;
    }

    public void setTxtHoraMensaje(TextView txtHoraMensaje) {
        this.txtHoraMensaje = txtHoraMensaje;
    }

    public TextView getTxtMensajeMensaje() {
        return txtMensajeMensaje;
    }

    public void setTxtMensajeMensaje(TextView txtMensajeMensaje) {
        this.txtMensajeMensaje = txtMensajeMensaje;
    }

    public CircleImageView getImgFotoPerfilMensaje() {
        return imgFotoPerfilMensaje;
    }

    public void setImgFotoPerfilMensaje(CircleImageView imgFotoPerfilMensaje) {
        this.imgFotoPerfilMensaje = imgFotoPerfilMensaje;
    }

    public ImageView getImgMensajeFoto() {
        return imgMensajeFoto;
    }

    public void setImgMensajeFoto(ImageView imgMensajeFoto) {
        this.imgMensajeFoto = imgMensajeFoto;
    }
}
