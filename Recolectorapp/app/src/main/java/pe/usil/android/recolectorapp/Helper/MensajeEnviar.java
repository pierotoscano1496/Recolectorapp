package pe.usil.android.recolectorapp.Helper;

import java.util.Map;

/**
 * Created by Piero on 07/11/2017.
 */

public class MensajeEnviar extends Mensaje {
    private Map hora;

    public MensajeEnviar() {
    }

    public MensajeEnviar(Map hora) {
        this.hora = hora;
    }

    public MensajeEnviar(String mensaje, String urlFoto, String nombre, String fotoPerfil, String tipoMensaje, boolean isResponse, Map hora) {
        super(mensaje, urlFoto, nombre, fotoPerfil, tipoMensaje, isResponse);
        this.hora = hora;
    }

    public MensajeEnviar(String mensaje, String nombre, String fotoPerfil, String tipoMensaje, boolean isResponse, Map hora) {
        super(mensaje, nombre, fotoPerfil, tipoMensaje, isResponse);
        this.hora = hora;
    }

    public Map getHora() {
        return hora;
    }

    public void setHora(Map hora) {
        this.hora = hora;
    }
}
