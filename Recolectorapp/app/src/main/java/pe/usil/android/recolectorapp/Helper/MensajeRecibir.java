package pe.usil.android.recolectorapp.Helper;

/**
 * Created by Piero on 07/11/2017.
 */

public class MensajeRecibir extends Mensaje {
    private long hora;

    public MensajeRecibir() {
    }

    public MensajeRecibir(long hora) {
        this.hora = hora;
    }

    public MensajeRecibir(String mensaje, String urlFoto, String nombre, String fotoPerfil, String tipoMensaje, boolean isResponse, long hora) {
        super(mensaje, urlFoto, nombre, fotoPerfil, tipoMensaje, isResponse);
        this.hora = hora;
    }

    public long getHora() {
        return hora;
    }

    public void setHora(long hora) {
        this.hora = hora;
    }
}
