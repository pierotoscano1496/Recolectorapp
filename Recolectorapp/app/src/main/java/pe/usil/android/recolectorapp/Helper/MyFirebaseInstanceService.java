package pe.usil.android.recolectorapp.Helper;

import android.util.Log;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;

/**
 * Created by Piero on 19/11/2017.
 */

public class MyFirebaseInstanceService extends FirebaseInstanceIdService {
    private static final String TAG = "Notificacion";

    @Override
    public void onTokenRefresh() {
        super.onTokenRefresh();

        String token = FirebaseInstanceId.getInstance().getToken();
        Log.d(TAG, "Token: " + token);

    }
}
