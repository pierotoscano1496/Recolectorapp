package pe.usil.android.recolectorapp;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ServerValue;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import ai.api.AIListener;
import ai.api.AIServiceException;
import ai.api.android.AIConfiguration;
import ai.api.android.AIDataService;
import ai.api.android.AIService;
import ai.api.model.AIError;
import ai.api.model.AIRequest;
import ai.api.model.AIResponse;
import ai.api.model.Result;
import de.hdodenhof.circleimageview.CircleImageView;
import pe.usil.android.recolectorapp.Helper.AdapterMensajes;
import pe.usil.android.recolectorapp.Helper.MensajeEnviar;
import pe.usil.android.recolectorapp.Helper.MensajeRecibir;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link PageChatting.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link PageChatting#newInstance} factory method to
 * create an instance of this fragment.
 */
public class PageChatting extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private AIService aiService;

    private CircleImageView imgFotoPerfil;
    private TextView txtNombre;
    private RecyclerView rvMensajes;
    private ImageButton btnEnviarFoto;
    private EditText etMensaje;
    private Button btnEnviarMensaje;
    private AdapterMensajes adapterMensajes;

    private FirebaseDatabase database;
    private DatabaseReference reference;
    private FirebaseStorage storage;
    private StorageReference storageReference;
    private static final int PHOTO_SEND = 191;
    private static final int PHOTO_PERFIL = 183;
    private FirebaseUser currentUser;

    private String fotoPerfilString;

    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    public PageChatting() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment PageChatting.
     */
    // TODO: Rename and change types and number of parameters
    public static PageChatting newInstance(String param1, String param2) {
        PageChatting fragment = new PageChatting();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_page_chatting, container, false);
        final AIConfiguration config = new AIConfiguration("ef3a336b8dc140e4b799a51e3e6726b8", AIConfiguration.SupportedLanguages.Spanish, AIConfiguration.RecognitionEngine.System);
        aiService = AIService.getService(getActivity(), config);
        aiService.setListener(new AIListener() {
            @Override
            public void onResult(AIResponse result) {

            }

            @Override
            public void onError(AIError error) {

            }

            @Override
            public void onAudioLevel(float level) {

            }

            @Override
            public void onListeningStarted() {

            }

            @Override
            public void onListeningCanceled() {

            }

            @Override
            public void onListeningFinished() {

            }
        });
        final AIDataService aiDataService = new AIDataService(getActivity(), config);
        final AIRequest aiRequest = new AIRequest();

        imgFotoPerfil = (CircleImageView) view.findViewById(R.id.imgFotoPerfil);
        txtNombre = (TextView) view.findViewById(R.id.txtNombre);
        rvMensajes = (RecyclerView) view.findViewById(R.id.rvMensajes);
        btnEnviarFoto = (ImageButton) view.findViewById(R.id.btnEnviarFoto);
        etMensaje = (EditText) view.findViewById(R.id.etMensaje);
        btnEnviarMensaje = (Button) view.findViewById(R.id.btnEnviarMensaje);

        database = FirebaseDatabase.getInstance();
        reference = database.getReference("chat");
        storage = FirebaseStorage.getInstance();

        adapterMensajes = new AdapterMensajes(getActivity());
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        rvMensajes.setLayoutManager(linearLayoutManager);
        rvMensajes.setAdapter(adapterMensajes);

        currentUser = ((Principal) getActivity()).getCurrentUser();
        txtNombre.setText(currentUser.getDisplayName());
        fotoPerfilString = currentUser.getPhotoUrl().toString();
        Glide.with(getActivity()).load(fotoPerfilString).into(imgFotoPerfil);

        btnEnviarMensaje.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String mensaje = etMensaje.getText().toString();
                String nombre = currentUser.getDisplayName();
                if (!mensaje.equals("")) {
                    MensajeEnviar msgEnviar = new MensajeEnviar(mensaje, nombre, fotoPerfilString, "1", false, ServerValue.TIMESTAMP);
                    aiRequest.setQuery(mensaje);
                    reference.child(currentUser.getUid()).push().setValue(msgEnviar);
                    new AsyncTask<AIRequest, Void, AIResponse>() {

                        @Override
                        protected AIResponse doInBackground(AIRequest... params) {
                            final AIRequest request = params[0];
                            try {
                                final AIResponse response = aiDataService.request(aiRequest);
                                return response;
                            } catch (AIServiceException e) {
                                e.printStackTrace();
                            }
                            return null;
                        }

                        @Override
                        protected void onPostExecute(AIResponse aiResponse) {
                            super.onPostExecute(aiResponse);
                            if (aiResponse != null) {
                                Result result = aiResponse.getResult();
                                String respuesta = result.getFulfillment().getSpeech();
                                MensajeEnviar msgEnviar = new MensajeEnviar(respuesta, "Soporte", "", "1", true, ServerValue.TIMESTAMP);
                                reference.child(currentUser.getUid()).push().setValue(msgEnviar);
                            }
                        }
                    }.execute(aiRequest);
                } else {
                    aiService.startListening();
                }
                etMensaje.setText("");
            }
        });

        btnEnviarFoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
                intent.setType("image/jpeg");
                intent.putExtra(Intent.EXTRA_LOCAL_ONLY, true);
                startActivityForResult(intent.createChooser(intent, "Seleccione una foto"), PHOTO_SEND);
            }
        });

        adapterMensajes.registerAdapterDataObserver(new RecyclerView.AdapterDataObserver() {
            @Override
            public void onItemRangeChanged(int positionStart, int itemCount) {
                super.onItemRangeChanged(positionStart, itemCount);
                setScrollBar();
            }
        });

        reference.child(currentUser.getUid()).addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                MensajeRecibir msgRecibido = dataSnapshot.getValue(MensajeRecibir.class);
                adapterMensajes.addMensaje(msgRecibido);
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        return view;
    }

    public void setScrollBar() {
        rvMensajes.scrollToPosition(adapterMensajes.getItemCount() - 1);
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == PHOTO_SEND && resultCode == getActivity().RESULT_OK) {
            Uri uri = data.getData();
            storageReference = storage.getReference("imagenes_chat");
            final StorageReference fotoReference = storageReference.child(uri.getLastPathSegment());
            fotoReference.putFile(uri).addOnSuccessListener(getActivity(), new OnSuccessListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                    Uri u = taskSnapshot.getDownloadUrl();
                    MensajeEnviar msgEnviar = new MensajeEnviar(currentUser.getDisplayName() + ", te envié una foto", u.toString(), currentUser.getDisplayName(), fotoPerfilString, "2", false, ServerValue.TIMESTAMP);
                    reference.child(currentUser.getUid()).push().setValue(msgEnviar);
                }
            });
        }
    }
}
